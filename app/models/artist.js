import DS from 'ember-data';

export default DS.Model.extend({
  uri: DS.attr('string'),
  permalink: DS.attr('string'),
  permalink_url: DS.attr('string'),
  avatar_url: DS.attr('string'),
  username: DS.attr('string'),
  full_name: DS.attr('string'),
  description: DS.attr('string')
});
