import DS from 'ember-data';

export default DS.Model.extend({
  playlist: DS.belongsTo('playlist'),
  duration: DS.attr('string'),
  artworkUrl: DS.attr('string'),
  title: DS.attr('string'),
  createdAt: DS.attr('date')
});
