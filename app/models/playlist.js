import DS from 'ember-data';

export default DS.Model.extend({
  tracks: DS.hasMany('track'),
  artworkUrl: DS.attr('string'),
  title: DS.attr('string'),
  createdAt: DS.attr('date')
});
