import Ember from 'ember';
import SC from '../utils/soundcloud';

export default Ember.Component.extend({
  isExpanded: false,
  actions: {
    expand() {
      Ember.$('.search-item-box').removeClass('is-expanded');
      this.toggleProperty('isExpanded');
      this.send('play');
    },
    play() {
      let track_url = this.$().find(".search-item-uri").text().replace( /^\D+/g, '');

      this.send('stopAll');

      SC.stream(`/tracks/${track_url}`, function(sound) {
        sound.play();
      });
    },
    stopAll() {
      if ( typeof(soundManager) !== 'undefined' ) {
        soundManager.stopAll();
      }
    }
  }
});
