import Ember from 'ember';

export default Ember.Controller.extend({
  needs: 'application',
  artistName: Ember.computed.alias('controllers.application.artistName')
  // styleBackground: function() {
    // var color = escapeCSS(this.get('color'));
  //   return ("background-image: " + color).htmlSafe();
  // }.property('color')
});
