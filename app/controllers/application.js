import Ember from "ember";

export default Ember.Controller.extend({
  actions: {
    doSearch() {

      if ( (this.get('currentPath') === "search") ) {
        // this.transitionToRoute('stream');
        // this.get('currentPath').refresh();
        this.get('target.router').refresh();
      } else {
        this.transitionToRoute('search');
      }

    }
  }
});