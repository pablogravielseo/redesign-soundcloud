import Ember from 'ember';

export default Ember.Controller.extend({
  needs: 'application',
  query: null,
  query2: '1293',
  actions: {
    doSearch: function(query) {
      console.log("active: shared input-search controller.");

      this.set('query', query);
      console.log(query);
      
      if ( !(this.get('controllers.application.currentPath') === "search") ) {
        this.transitionToRoute('search');
        // this.transitionToRoute('search', { query: query });
      }


    }
  }
});
