import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

export default Router.map(function() {
  this.route('stream', { path: '/' });
  this.route('explore');
  this.route('search');
  this.resource('artists', function() {
    this.route('show', { path: ':artist_id' });
  });
});
