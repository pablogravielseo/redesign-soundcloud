import Ember from 'ember';
import SC from '../utils/soundcloud';
import secrets from '../utils/soundcloud-api';

export default Ember.Route.extend({
  beforeModel() {
    // Initialize soundcloud before hitting API
    SC.initialize({
      client_id: secrets.soundcloud_api_key
    });
  },
  model() {
    let artistName = this.controllerFor("search").get("artistName");
    
    return new Ember.RSVP.Promise(function(resolve) {
      SC.get('/tracks', { q: artistName, limit: 10 }, function(tracks) {
        console.log(tracks);
        resolve(tracks);
      });
    });
    
  }
});