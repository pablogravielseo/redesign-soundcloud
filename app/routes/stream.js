import Ember from 'ember';
import SC from '../utils/soundcloud';
import secrets from '../utils/soundcloud-api';

export default Ember.Route.extend({
  beforeModel: function() {    
    // Initialize soundcloud before hitting API
    SC.initialize({
      client_id: secrets.soundcloud_api_key
    });
  },
  model: function() {
    // return SC.get("/groups/55517/tracks", {limit: 1}, function(tracks) {
    //     alert("Latest track: " + tracks[0].title);
    //   });

    // return SC.get("/tracks", {q:'293', limit: 5}, function(tracks) {
    //     alert("Latest track: " + tracks[0].title);
    //     console.log(tracks);
    //   });
  }
});
