import Ember from 'ember';
import SC from '../../utils/soundcloud';
import secrets from '../../utils/soundcloud-api';

export default Ember.Route.extend({
  beforeModel: function() {
    
    // Initialize soundcloud before hitting API
    SC.initialize({
      client_id: secrets.soundcloud_api_key
    });

  },
  model: function() {
    
    var userId = 39090345; // user_id of Prutsonic

    return SC.get("/users/" + userId, function(tracks) {
      console.log(tracks.username);
      console.log(tracks.full_name);
      console.log(tracks.permalink_url);
    });

    // Prutsonic = SC.get("/tracks", {
    //   user_id: userId,
    //   limit: 30
    // }, function(tracks) {
    //   for (var i = 0; i < tracks.length; i++) {
    //     console.log(tracks[i].title);
    //   }
    // });


    // return Prutsonic;

  }
});
