import Ember from 'ember';

export default Ember.TextField.extend({
  tagName: 'input',
  type: 'text',
  classNames: 'form-control form-control-search',
  attributeBindings: ['placeholder'],
  placeholder: 'Search...',
  value: ''
});